import 'package:flutter/material.dart';

class NavList {
  String? title;
  Widget? pageNames;
  String? imageIcon;
  String? imageIconSelected;
  bool? toShow;

  NavList({
    required this.title,
    required this.pageNames,
    required this.imageIcon,
    required this.imageIconSelected,
    required this.toShow
  });
}

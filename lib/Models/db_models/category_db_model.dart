const String tblCategory = 'tbl_category';

class CategoryFields {
  static final List<String> values = [
    id,
    categoryName,
    isFavourite,
  ];

  static const String id = 'id';
  static const String categoryName = 'categoryName';
  static const String isFavourite = 'isFavourite';
}

class CategoryDBModel {
  final int? id;
  final String categoryName;
  final int isFavourite;

  const CategoryDBModel({
    this.id,
    required this.categoryName,
    required this.isFavourite,
  });

  static CategoryDBModel fromJson(Map<String, Object?> json) => CategoryDBModel(
        id: json[CategoryFields.id] as int,
        categoryName: json[CategoryFields.categoryName] as String,
        isFavourite: json[CategoryFields.isFavourite] as int,
      );

  Map<String, Object?> toJson() => {
        CategoryFields.id: id,
        CategoryFields.categoryName: categoryName,
        CategoryFields.isFavourite: isFavourite,
      };
}

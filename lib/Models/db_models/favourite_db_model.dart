const String tblFavourite = 'tbl_favourite';

class FavouriteFields {
  static final List<String> values = [
    id,
    categoryName,
    isFavourite,
    catId
  ];

  static const String id = 'id';
  static const String categoryName = 'categoryName';
  static const String isFavourite = 'isFavourite';
  static const String catId = 'categoryId';
}

class FavouriteDBModel {
  final int? id;
  final String FavCategoryName;
  final int isFavourite;
  final int catId;

  const FavouriteDBModel({
    this.id,
    required this.FavCategoryName,
    required this.isFavourite,
    required this.catId
  });

  static FavouriteDBModel fromJson(Map<String, Object?> json) => FavouriteDBModel(
        id: json[FavouriteFields.id] as int,
        FavCategoryName: json[FavouriteFields.categoryName] as String,
        isFavourite: json[FavouriteFields.isFavourite] as int,
        catId: json[FavouriteFields.catId] as int,
      );

  Map<String, Object?> toJson() => {
        FavouriteFields.id: id,
        FavouriteFields.categoryName: FavCategoryName,
        FavouriteFields.isFavourite: isFavourite,
        FavouriteFields.catId: catId,
      };
}

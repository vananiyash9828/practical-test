import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

import '../../Models/db_models/category_db_model.dart';
import '../../Models/db_models/favourite_db_model.dart';
class AppDatabase {
  Database? _database;
  static AppDatabase? _appDatabase;

  _AppDatabase() {}

  static AppDatabase getInstance() {
    _appDatabase ??= AppDatabase();
    return _appDatabase!;
  }

  static void makeDBObjectNull() {
    _appDatabase = null;
  }

  Future<Database> databaseTry() async {
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "practialApp.db");


    return await openDatabase(path, version: 1);
  }

  Future<Database> get database async {
    if (_database != null) return _database!;
    
     _database = await _initDB('practialApp.db');
    return _database!;
  }

  Future<Database> _initDB(String filePath) async {
    final dbPath = await getDatabasesPath();
    final path = join(dbPath, filePath);
    print('Database path $path');

    return await openDatabase(path, version: 1, onCreate: _createDB);
  }

    Future _createDB(Database db, int version) async {
    const idType = 'INTEGER PRIMARY KEY AUTOINCREMENT';
    const textType = 'TEXT NOT NULL';
    const text = 'TEXT';
    const boolType = 'BOOLEAN NOT NULL';
    const integerType = 'INTEGER NOT NULL';
    const intType = 'INTEGER';
    const textTypeNull = 'TEXT';

    await db.execute('''
CREATE TABLE $tblCategory (
  ${CategoryFields.id} $idType,
  ${CategoryFields.categoryName} $textType,
  ${CategoryFields.isFavourite} $integerType
  )
  ''');

  await db.execute('''
CREATE TABLE $tblFavourite (
  ${FavouriteFields.id} $idType,
  ${FavouriteFields.categoryName} $textType,
  ${FavouriteFields.isFavourite} $integerType,
  ${FavouriteFields.catId} $integerType
  )
  ''');
  }
}

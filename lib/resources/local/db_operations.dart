import 'dart:developer';

import 'package:practical_test/Models/db_models/category_db_model.dart';
import 'package:practical_test/Models/db_models/favourite_db_model.dart';
import 'package:sqflite/sqflite.dart';

import 'app_database.dart';

class DBOperations {
  static Database? _db;

  static final DBOperations _dbOperations = DBOperations._internal();

  factory DBOperations() {
    return _dbOperations;
  }

  Future<void> initDB() async {
    _db = await (AppDatabase.getInstance()).database;
    print("_db :: $_db");
  }

  DBOperations._internal();

  Future<void> createCategoryDetails(CategoryDBModel categoryDBModel) async{
    _db = await (AppDatabase.getInstance()).database;
    log("_db >>>>:: $_db");
    final id = await _db!.insert(tblCategory, categoryDBModel.toJson());
    log('createRentAppDetails insert = $id');
  }

  Future<List<CategoryDBModel>> readAllCategoryDetails() async { 
    _db = await (AppDatabase.getInstance()).database;
    String orderBy;
    orderBy = '${CategoryFields.id} ASC';
    final result = await _db!.query(tblCategory, orderBy: orderBy);
    return result.map((json) => CategoryDBModel.fromJson(json)).toList();
  }

   Future<void> favouriteAdd(FavouriteDBModel favouriteDBModel) async{
    _db = await (AppDatabase.getInstance()).database;
    log("_db >>>>:: $_db");
    final id = await _db!.insert(tblFavourite, favouriteDBModel.toJson());
    log('favouriteAdd insert = $id');
  }

  Future<List<FavouriteDBModel>> readAllFavourites() async { 
    _db = await (AppDatabase.getInstance()).database;
    String orderBy;
    orderBy = '${FavouriteFields.id} ASC';
    final result = await _db!.query(tblFavourite, orderBy: orderBy);
    return result.map((json) => FavouriteDBModel.fromJson(json)).toList();
  }

  Future<int> updateCategoryList(int id, CategoryDBModel categoryDBModel,isFavourite) async {
    var valueMap = {
      CategoryFields.isFavourite: isFavourite,
      
      
    };
    return await _db!.update(tblCategory, valueMap,
        where: '${CategoryFields.id} = ?', whereArgs: [id]);
  }

  Future<int> updateFavList(int id, FavouriteDBModel favouriteDBModel,isFavourite) async {
    var valueMap = {
      FavouriteFields.isFavourite: isFavourite,  
    };
    return await _db!.update(tblFavourite, valueMap,
        where: '${FavouriteFields.id} = ?', whereArgs: [id]);
  }

  Future<int?> deleteFavourite(int id) async{
    var IdExist = await _db!.query('$tblFavourite',
        where: "${FavouriteFields.id} = '$id'");
    if (IdExist != null || IdExist.isEmpty) {

      print('appointment deleted $id');
      return await _db!.delete(tblFavourite,
          where: '${FavouriteFields.id} = ?', whereArgs: [id]);
    }}

}
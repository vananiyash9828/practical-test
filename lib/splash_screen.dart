import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:practical_test/Dashboard/Home/dahsboard_screen.dart';
import 'package:practical_test/Utils/image_path_constants.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Future.delayed(const Duration(seconds: 2), () {
      Navigator.pushAndRemoveUntil(
          context,
          CupertinoPageRoute(builder: (context) => const DashBoardScreen()),
          (route) => false);
    });
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    return Scaffold(
      body: Container(
        width: width,
        height: height,
        color: Colors.white,
        alignment: Alignment.center,
        child: Image.asset(ImagePath.logoImage),
      ),
    );
  }
}

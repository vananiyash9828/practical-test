import 'package:flutter/material.dart';
import 'package:practical_test/Dashboard/DrawerScreen/drawer_screen.dart';
import 'package:practical_test/Utils/app_constants.dart';

import '../../Utils/image_path_constants.dart';
import '../Category/category_screen.dart';
import '../Favourite/favourite_screen.dart';

class DashBoardScreen extends StatefulWidget {
  const DashBoardScreen({Key? key}) : super(key: key);

  @override
  State<DashBoardScreen> createState() => _DashBoardScreenState();
}

class _DashBoardScreenState extends State<DashBoardScreen> with TickerProviderStateMixin{
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  TabController? tabController;

  @override
  void initState() {
    super.initState();
    tabController = TabController(length: 2, vsync: this);
  }
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          leading: Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: IconButton(
              icon: Image.asset(
                ImagePath.menuIcon,
                color: Colors.white,
              ),
              onPressed: () {
                _scaffoldKey.currentState!.openDrawer();
              },
            ),
          ),
          centerTitle: true,
          title: const Text(AppConstants.categoryList),
          bottom:  TabBar(
            controller: tabController,
            tabs: const [
              Tab(
                text: AppConstants.category,
              ),
              Tab(
                text: AppConstants.favourite,
              ),
            ],
          ),
        ),
        drawer: const DrawerScreen(),
        body: TabBarView(
          controller: tabController,
          children: const [
          CategoryScreen(),
          FavouriteScreen()
          ]),
      ),
    );
  }
}

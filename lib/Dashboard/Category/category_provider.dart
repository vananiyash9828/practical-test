import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:practical_test/Models/db_models/category_db_model.dart';
import 'package:practical_test/Models/db_models/favourite_db_model.dart';
import 'package:practical_test/Utils/api_constants.dart';
import 'package:http/http.dart' as http;

import '../../Models/category_model.dart';
import '../../resources/local/db_operations.dart';

class CategoryProvider extends ChangeNotifier {
  final DBOperations _db = DBOperations();
  CategoryDetails? _categoryDetails;
  CategoryDetails? get categoryDetails => _categoryDetails;
  set categoryDetails(CategoryDetails? value) {
    _categoryDetails = value;
    notifyListeners();
  }

  bool _isLoading = false;
  bool get isLoading => _isLoading;
  set isLoading(bool value) {
    _isLoading = value;
    notifyListeners();
  }

  int _selectIndex = 0;
  int get selectIndex => _selectIndex;
  set selectIndex(int value) {
    _selectIndex = value;
    notifyListeners();
  }

  List<CategoryDBModel> _categorySelection = [];

  List<CategoryDBModel> get categorySelection => _categorySelection;

  set categorySelection(List<CategoryDBModel> value) {
    _categorySelection = value;
  }

  fetchDataFromDB() async{
    isLoading = true;
    categorySelection = await  _db.readAllCategoryDetails();
    isLoading = false;

    if(categorySelection.isEmpty){
      getCategoryList();
    }
    notifyListeners();
  }

  Future<void> getCategoryList() async {
    final uri = Uri.parse(Api.categoryUrl);

    try {
      isLoading = true;
      final response = await http.get(uri);
      if (response.statusCode == 200) {
        if (response.body.isNotEmpty) {
          var result = jsonDecode(response.body);
          categoryDetails = CategoryDetails.fromJson(result);
          if (categoryDetails != null) {
            for (var i = 0; i < categoryDetails!.entries!.length; i++) {
              insertDataInDb(categoryDetails!.entries![i].aPI);
            }
          }
          notifyListeners();
        }
      }
      notifyListeners();
    } catch (e) {
      isLoading = false;
      log("Exception $e");
    }
  }

  insertDataInDb(catName) async {
    final categoryDetailsList = CategoryDBModel(
      categoryName: catName,
      isFavourite: 0,
    );
    await DBOperations().createCategoryDetails(categoryDetailsList).then((value) => fetchDataFromDB());
    notifyListeners();
  }

  insertFavouriteList(FavouriteDBModel favouriteDBModel) async{
    isLoading = true;
    await DBOperations().favouriteAdd(favouriteDBModel).then((value) => fetchDataFromDB());
    isLoading = false;
    notifyListeners();
  }

  Future<void> updateData(id,CategoryDBModel categoryDBModel,int isFavourite) async{
    isLoading = true;
    await _db.updateCategoryList(id, categoryDBModel, isFavourite).then((value) {
    });
    isLoading = false;
    notifyListeners();
  }  
}

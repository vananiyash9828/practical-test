import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import 'package:practical_test/Models/db_models/category_db_model.dart';
import 'package:practical_test/Models/db_models/favourite_db_model.dart';
import 'package:practical_test/Utils/image_path_constants.dart';
import 'package:provider/provider.dart';

import 'category_provider.dart';

class CategoryScreen extends StatefulWidget {
  const CategoryScreen({Key? key}) : super(key: key);

  @override
  State<CategoryScreen> createState() => _CategoryScreenState();
}

class _CategoryScreenState extends State<CategoryScreen> {
  final CategoryProvider _categoryProvider = CategoryProvider();
  int isFavourite = 0;
  @override
  void initState() {
    super.initState();
    _categoryProvider.fetchDataFromDB();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<CategoryProvider>(
      create: (context) => _categoryProvider,
      child: WillPopScope(
        onWillPop: _onBackPressed,
        child: Consumer<CategoryProvider>(builder: (context, model, child) {
          return ModalProgressHUD(
            inAsyncCall: model.isLoading,
            child: ListView.builder(
                itemCount: model.categorySelection.length,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    onTap: () {
                      setState(() {
                        if (isFavourite == 0) {
                          isFavourite = 1;
                        } else {
                          isFavourite = 0;
                        }
                        updateData(model,index,isFavourite);
                      });
                    },
                    child: ListTile(
                      title: Text(model.categorySelection != null
                          ? model.categorySelection[index].categoryName
                              .toString()
                          : ""),
                      trailing: Image.asset(
                        model.categorySelection[index].isFavourite == 1
                            ? ImagePath.favouriteIcon
                            : ImagePath.unFavouriteIcon,
                        height: 25,
                      ),
                    ),
                  );
                }),
          );
        }),
      ),
    );
  }

  Future<bool> _onBackPressed() {
    exit(0);
  }

  updateData(CategoryProvider model, index, isFavouriteUpdate) {
    setState(() {
      model
          .updateData(
              model.categorySelection[index].id!,
              model.categorySelection[index],
              model.categorySelection[index].isFavourite == 0 ? 1 : 0)
          .then((value) {
        final favList = FavouriteDBModel(
            FavCategoryName: model.categorySelection[index].categoryName,
            isFavourite:
                model.categorySelection[index].isFavourite == 0 ? 1 : 0,
            catId: model.categorySelection[index].id!);
          model.insertFavouriteList(favList);        
      });
    });
  }
}

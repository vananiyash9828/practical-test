import 'dart:io';

import 'package:flutter/material.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import 'package:practical_test/Dashboard/Favourite/favourite_provider.dart';
import 'package:practical_test/Models/db_models/favourite_db_model.dart';
import 'package:practical_test/Utils/image_path_constants.dart';
import 'package:provider/provider.dart';

class FavouriteScreen extends StatefulWidget {
  const FavouriteScreen({Key? key}) : super(key: key);

  @override
  State<FavouriteScreen> createState() => _FavouriteScreenState();
}

class _FavouriteScreenState extends State<FavouriteScreen> {
  final FavouriteProvider _favouriteProvider = FavouriteProvider();

  @override
  void initState() {
    super.initState();
    _favouriteProvider.fetchFavouriteList();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<FavouriteProvider>(
      create: (context) => _favouriteProvider,
      child: WillPopScope(
        onWillPop: _onBackPressed,
        child: Consumer<FavouriteProvider>(builder: (context, model, child) {
          return ModalProgressHUD(
            inAsyncCall: model.isLoading,
            child: ListView.builder(
              itemCount: model.favouriteList.length,
              itemBuilder: (context,index){
                return GestureDetector(
                    onTap: () {
                        setState(() {
                          final favList = FavouriteDBModel(
                            id: model.favouriteList[index].id,
                            FavCategoryName:
                                model.favouriteList[index].FavCategoryName,
                            isFavourite: model.favouriteList[index].isFavourite == 0? 1 :0,
                            catId: model.favouriteList[index].catId);
                        model.updateFavData(
                            model.favouriteList[index].id, favList);
                        });
                      },
                    child: ListTile(
                  title: Text(model.favouriteList[index].FavCategoryName),
                  trailing: Image.asset(
                    model.favouriteList[index].isFavourite == 1
                        ? ImagePath.favouriteIcon
                        : ImagePath.unFavouriteIcon,
                    height: 25,
                  ),
                ),
              );
              }
            ),
          );
        }),
      )
    );
  }
  Future<bool> _onBackPressed() {
    exit(0);
  }
}
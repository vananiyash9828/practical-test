import 'package:flutter/material.dart';
import 'package:practical_test/Models/db_models/category_db_model.dart';
import 'package:practical_test/Models/db_models/favourite_db_model.dart';
import 'package:practical_test/resources/local/db_operations.dart';

class FavouriteProvider extends ChangeNotifier {
 final DBOperations _db = DBOperations(); 

 bool _isLoading = false;
  bool get isLoading => _isLoading;
  set isLoading(bool value) {
    _isLoading = value;
    notifyListeners();
  }

  List<FavouriteDBModel> _favouriteList = [];

  List<FavouriteDBModel> get favouriteList => _favouriteList;

  set favouriteList(List<FavouriteDBModel> value) {
    _favouriteList = value;
    notifyListeners();
  }

  fetchFavouriteList() async{
    favouriteList = await _db.readAllFavourites();
    notifyListeners();
  }

  Future<void> updateFavData(id,FavouriteDBModel favouriteDBModel) async{
    isLoading = true;
    await _db.updateFavList(id, favouriteDBModel, favouriteDBModel.isFavourite == 0 ? 1 :0).then((value) {
      if(favouriteDBModel.isFavourite == 0){
        final catList = CategoryDBModel(
          categoryName: favouriteDBModel.FavCategoryName, 
          isFavourite: favouriteDBModel.isFavourite);
        _db.updateCategoryList(favouriteDBModel.catId, catList, favouriteDBModel.isFavourite);
        _db.deleteFavourite(id).then((value) => fetchFavouriteList());
        notifyListeners();
      }
    });
    isLoading = false;
    notifyListeners();
  }

}
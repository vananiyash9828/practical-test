import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:practical_test/Dashboard/Home/dahsboard_screen.dart';
import 'package:practical_test/Utils/app_constants.dart';
import 'package:practical_test/Utils/text_styles.dart';

import '../../Models/navList_model.dart';
import '../../Utils/image_path_constants.dart';

class DrawerScreen extends StatefulWidget {
  const DrawerScreen({Key? key}) : super(key: key);

  @override
  State<DrawerScreen> createState() => _DrawerScreenState();
}

class _DrawerScreenState extends State<DrawerScreen> {
  List<NavList> navList = [
    NavList(
      title: AppConstants.categoryList, 
      pageNames: const DashBoardScreen(), 
      imageIcon: ImagePath.categoryIcon, 
      imageIconSelected: "", 
      toShow: true
    ),
    NavList(
      title: AppConstants.favouriteList, 
      pageNames: const DashBoardScreen(), 
      imageIcon: ImagePath.favouriteIcon, 
      imageIconSelected: "", 
      toShow: true
    )
  ];

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return SafeArea(
      child: Drawer(
        child: Column(
          children: [
            Container(
              width: width,
              height: 150,
              color: Colors.blueGrey,
              alignment: Alignment.center,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(width: 16,),
                  const CircleAvatar(
                    backgroundImage:  AssetImage(ImagePath.userIcon)
                  ),
                  SizedBox(width: 16,),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(AppConstants.userName,style: titleT1,)
                    ],
                  ),
                  SizedBox(width: 16,),
                ],
              ),
            ),
            ListView.builder(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemCount: navList.length,
              itemBuilder: (context,index){
                return GestureDetector(
                  onTap: (){
                    Navigator.pushAndRemoveUntil(context, CupertinoPageRoute(builder: (context)=>navList[index].pageNames!), (route) => false);
                  },
                  child: ListTile(
                    leading: Image.asset(navList[index].imageIcon!,height: 20,),
                    title: Text(navList[index].title.toString()),
                  ),
                );
              }
            ),
          ],
        ),
      ),
    );
  }
}
class ImagePath {
  
  static const String logoImage = 'Assets/images/splash_logo.png';
  static const String menuIcon = 'Assets/images/menu.png';
  static const String categoryIcon = 'Assets/images/category.png';
  static const String favouriteIcon = 'Assets/images/favourite.png';
  static const String unFavouriteIcon = 'Assets/images/unFavourite.png';
  static const String userIcon = 'Assets/images/user.png';

}
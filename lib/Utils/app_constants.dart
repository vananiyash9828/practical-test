class AppConstants {

  static const String categoryList = 'Category List';
  static const String favouriteList = 'Favourite Items';
  static const String category = 'Category';
  static const String favourite = 'Favourite';
  static const String userName = 'Yash Vanani';
  
}
